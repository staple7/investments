/*
 * contact.js
 */

var RecaptchaOptions = { theme : 'custom', custom_theme_widget: 'recaptcha_widget' };

function highlight_errors(fields) {
    $(".form-group").each(function() { $(this).removeClass("has-error"); });
    for (var i = 0; i < fields.length; i++) $("#"+fields[i]).parent().parent(".form-group").addClass("has-error");
}

function new_captcha() {
    $.post( "includes/new_captcha.php", "", function(data) {
        $("span#captcha_question").html(data);
        $("input#captcha").val('');
    }, "html").fail(function() {
        alert("There has been a problem with the captcha verification. Please refresh the page or email us directly.");
    });
}

function manage_errors(fields) {
    highlight_errors(fields);
    new_captcha();
    $("form#contact_form #"+fields[0]).focus();
}

function reset_form() {
    highlight_errors(["Gibberish"]);
    new_captcha();
    $("form#contact_form input, form#contact_form textarea").each(function() { if ($(this).attr("id") != "submit") $(this).val(''); });
    $("form#contact_form .form-group").css("display","none");
}

function manage_message(message) {
    $("form#contact_form .alert").each(function() {
        $(this).css("display","none");
    });
    $("form#contact_form #form-"+message).css("display","block");
}

$( "form#contact_form #submit" ).click(function() {
    var required_fields = [];
    $("input[required], textarea[required]").each(function() {
        if ($(this).val() == null || $(this).val() == "") required_fields.push($(this).attr("id"));
    });

    if (required_fields.length > 0) {
        new_captcha();
        highlight_errors(required_fields);
        manage_message("danger");
    } else {
        manage_message("info");
        var form_data = $( "form#contact_form" ).serialize();
        $("form#contact_form input, form#contact_form textarea").each(function() { $(this).prop("disabled",true) });
        $.post( "includes/contact_process.php", form_data, function(data) {
            $("form#contact_form input, form#contact_form textarea").each(function() { $(this).prop("disabled",false) });
            if (data == "ok") {
                manage_message("success");
                reset_form();
            } else if (data == "fail") {
                manage_message("warning");
                reset_form();
            } else {
                manage_errors(data.split(","));
                manage_message("danger");
            }
        }, "html").fail(function() {
            alert("There has been a problem submitting your message. Please email us directly.");
        });
    }
});
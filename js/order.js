/*
 * order.js
 */

var BigPictureCheckout = function (order_form) {
    /* CONSTRUCTOR */
    /* Auto-load constructor */
    if (!(this instanceof BigPictureCheckout)) { return new BigPictureCheckout(order_form); }

    /* VARS */
    var _this = this;
    this.order_form = "form#" + order_form;
    this.submit_button = this.order_form + " #submit_btn";
    this.h_quantity = 0;
    this.w_quantity = 0;

    this.subtotal_selectors = {
        "current" : "#subtotal_current",
        "current_no_bulk" : "#subtotal_current_no_bulk",
        "preorder" : "#subtotal_preorder",
        "preorder_no_bulk" : "#subtotal_preorder_no_bulk",
        "final" : "#subtotal_final",
        "savings" : "#subtotal_savings"
    }

    this.yearKeyword = "<?php echo $ii_website->getYearKeyword(); ?>";
    this.productsKeyword = "<?php echo $ii_website->getConfigProductsKeyword(); ?>";
    this.productDisplayKeyword = "<?php echo $ii_website->getProductDisplayKeyword(); ?>";

    this.current_year = <?php echo $ii_website->getCurrentYear(); ?>;
    this.preorder_year = <?php echo $ii_website->getPreorderYear(); ?>;
    this.preorder_discount = <?php echo $ii_website->getPreorderDiscount(); ?>;
    this.pricing_breakdown = <?php echo json_encode($ii_website->getPricingBreakdown()); ?>;
    this.maxValuesByProductType = <?php echo json_encode($ii_website->getProductTypeMaxByProductType()); ?>;
    this.region_products = <?php echo json_encode($ii_website->getRegionProductsCombinationsAsObjects()); ?>;
    this.region_products_map_by_product_key = {};
    this.region_products_map_by_year = {};
    this.region_products_map_by_product_type = {};
    this.product_quantities = {};
    this.validations_by_year_and_product_type = {};

    this.init = function() {
        this.createRegionProductsMaps();
        this.setListeners();
        this.resetFormInputs();
        this.resetQuantitiesObject();
        this.tempFixForCanada();
    }

    /* METHODS */
    this.createRegionProductsMaps = function () {
        /* Create map by year */
        for (var i = 0; i < this.region_products.length; i++) {
            if (!this.region_products_map_by_year.hasOwnProperty(this.region_products[i][this.yearKeyword])) {
                this.region_products_map_by_year[this.region_products[i][this.yearKeyword]] = [];
                this.validations_by_year_and_product_type[this.region_products[i][this.yearKeyword]] = {};
            }
            this.region_products_map_by_year[this.region_products[i][this.yearKeyword]].push(this.region_products[i][this.productDisplayKeyword]);
        }   
        /* Create map by product type */
        for (var i = 0; i < this.region_products.length; i++) {
            if (!this.region_products_map_by_product_type.hasOwnProperty(this.region_products[i][this.productsKeyword]))
                this.region_products_map_by_product_type[this.region_products[i][this.productsKeyword]] = [];
            this.region_products_map_by_product_type[this.region_products[i][this.productsKeyword]].push(this.region_products[i][this.productDisplayKeyword]);
        }

        /* Create map by product key */
        for (var i = 0; i < this.region_products.length; i++)
            this.region_products_map_by_product_key[this.region_products[i][this.productDisplayKeyword]] = i;
    }

    this.getProductKeysAsString = function () {
        return "#" + Object.keys(this.region_products_map_by_product_key).join(",#");
    }

    this.getProductKeysOfSameProductType = function (product_type) {
        return this.region_products_map_by_product_type[product_type];
    }

    this.getProductKeysOfSameYearAndProductType = function (product_type, year) {
        var productKeysOfSameProductType = this.getProductKeysOfSameProductType(product_type);
        var productKeysOfSameYearAndProductType = [];
        for (var product_key_index in productKeysOfSameProductType)
            if (this.region_products_map_by_year[year].indexOf(productKeysOfSameProductType[product_key_index]) !== -1)
                productKeysOfSameYearAndProductType.push(this.getProductKeysOfSameProductType(product_type)[product_key_index]);
        return productKeysOfSameYearAndProductType;
    }

    this.getYearByProductKey = function (product_key) {
        return this.region_products[this.region_products_map_by_product_key[product_key]][this.yearKeyword];
    }

    this.getProductTypeByProductKey = function (product_key) {
        return this.region_products[this.region_products_map_by_product_key[product_key]][this.productsKeyword];
    }

    this.getProductTypeInYearTotalQuantity = function (product_type, year) {
        var sum = 0;
        var productKeysOfSameYearAndProductType = this.getProductKeysOfSameYearAndProductType(product_type, year);
        for (var product_key_index in productKeysOfSameYearAndProductType)
            sum += this.product_quantities[productKeysOfSameYearAndProductType[product_key_index]];
        return sum;
    }

    this.getUnitaryPriceByProductTypeAndQuantity = function (product_type, quantity) {
        for (var pricing_node_index in this.pricing_breakdown) {
            var pricing_node = this.pricing_breakdown[pricing_node_index];
            if (pricing_node.hasOwnProperty(product_type) && quantity >= pricing_node["min"] && quantity <= pricing_node["max"]) {
                return pricing_node[product_type];
            }
        }
        return 0;
    }

    this.getProductTypeMax = function (product_code) {
        return this.maxValuesByProductType[product_code];
    }

    this.extractIntegerValue = function (string) {
        return Math.round( parseFloat(string.replace(/\D/g, '')) || 0 );
    }

    this.validateValueWithinLimits = function (current_value, min_value, max_value) {
        current_value = Math.round( parseFloat(current_value) || 0 );
        if (current_value > max_value) current_value = max_value;
        else if(current_value < 0) current_value = 0;
        return current_value;
    }

    this.resetFormInputs = function () {
        for (var region_product_key in this.region_products_map_by_product_key)
            $( "#" + region_product_key ).val(0);
    }

    this.resetQuantitiesObject = function () {
        for (var product_key in this.region_products_map_by_product_key)
            this.product_quantities[product_key] = 0;
    }

    this.updateProductQuantityObjectByProductKey = function (product_key, value) {
        this.product_quantities[product_key] = value;
    }

    this.calculateSubtotalByYear = function (year, bulk_discount) {
        var bulk_discount = typeof bulk_discount !== 'undefined' ? bulk_discount : true;
        var subtotal = 0;
        for (var product_type in this.region_products_map_by_product_type) {
            var productTypeInYearTotalQuantity = this.getProductTypeInYearTotalQuantity(product_type, year);
            var unitary_price = this.getUnitaryPriceByProductTypeAndQuantity(product_type, ((bulk_discount === true) ? productTypeInYearTotalQuantity : 1) );

            if (year == this.current_year) {
                subtotal += unitary_price * productTypeInYearTotalQuantity;
            } else {
                subtotal += unitary_price * (1 - this.preorder_discount) * productTypeInYearTotalQuantity;
            }   
        }
        return subtotal;
    }

    this.calculateSavings = function (subtotal_with_savings, subtotal_without_savings) {
        var savings_text = "Order more charts and save";
        var savings = subtotal_without_savings - subtotal_with_savings;

        if (savings > 0) {
            if (savings > subtotal_with_savings) {
                // percentage
                savings = (1 - (subtotal_with_savings / subtotal_without_savings)) * 100;
                savings_text = Math.round(savings) + "%";
            } else {
                // dollars
                savings_text = _this.formatCurrency(savings);
            }
            savings_text = "Total volume savings: " + savings_text;
        }
               // console.log(subtotal_without_savings+"-"+subtotal_with_savings+"="+savings_text);

        return savings_text;
    }

    this.updateSubtotal = function (isValid) {
        if (isValid) {
            var current_subtotal = this.calculateSubtotalByYear(this.current_year);
            var preorder_subtotal = 0 ;
            var final_subtotal = current_subtotal + preorder_subtotal;

            var current_subtotal_no_bulk = this.calculateSubtotalByYear(this.current_year, false);
           var preorder_subtotal_no_bulk = 0;
            var final_subtotal_no_bulk = current_subtotal_no_bulk + preorder_subtotal_no_bulk;

            $(this.subtotal_selectors["current"]).text(this.formatCurrency(current_subtotal));
            $(this.subtotal_selectors["preorder"]).text(this.formatCurrency(preorder_subtotal));
            $(this.subtotal_selectors["savings"]).text(this.calculateSavings(final_subtotal, final_subtotal_no_bulk));
            $(this.submit_button).prop("disabled", final_subtotal == 0);

            $(this.subtotal_selectors["current_no_bulk"]).text(function() {
                if (current_subtotal_no_bulk > 0 && current_subtotal_no_bulk !== current_subtotal)
                    return _this.formatCurrency(current_subtotal_no_bulk);
                else return "";
            });

            $(this.subtotal_selectors["preorder_no_bulk"]).text(function() {
                if (preorder_subtotal_no_bulk > 0 && preorder_subtotal_no_bulk !== preorder_subtotal)
                    return _this.formatCurrency(preorder_subtotal_no_bulk);
                else return "";
            });
            
        } else {
            $(this.subtotal_selectors["current"]).text("- -");
            $(this.subtotal_selectors["preorder"]).text("- -");
            $(this.subtotal_selectors["savings"]).text("");
            $(this.submit_button).prop("disabled", true);
            $(this.subtotal_selectors["current_no_bulk"]).text("");
            $(this.subtotal_selectors["preorder_no_bulk"]).text("");
        }
    }

    this.ifFormValid = function() {
        for (var year in this.validations_by_year_and_product_type)
            for (var product_key in this.validations_by_year_and_product_type[year])
                if (this.validations_by_year_and_product_type[year][product_key] === false)
                    return false;
        return true;
    }
    
    this.formatCurrency = function (number) {
        var currencySymbol = '$';
        var thousandsSeparator = ',';
        var decimalSeparator = '.';

        number = (isNaN(number) || number == '' || number == null) ? 0.00 : number;
        var numberStr = parseFloat(number).toFixed(2).toString();
        var numberFormatted = new Array(decimalSeparator, numberStr.slice(-2)); /* this returns the decimal and cents*/
        numberStr = numberStr.substring(0, numberStr.length-3); /* this removes the decimal and cents*/

        while (numberStr.length > 3) {
            numberFormatted.unshift(numberStr.slice(-3)); /* this prepends the last three digits to `numberFormatted`*/
            numberFormatted.unshift(thousandsSeparator); /* this prepends the thousandsSeparator to `numberFormatted`*/
            numberStr = numberStr.substring(0, numberStr.length-3);  /* this removes the last three digits*/
        }
        numberFormatted.unshift(numberStr); /* there are less than three digits in numberStr, so prepend them*/
        numberFormatted.unshift(currencySymbol); /* add currencySymbol*/

        return numberFormatted.join(''); /* put it all together */
    }

    /* LISTENERS */
    this.setListeners = function () {
        /* Listen for input changes */
        $(_this.getProductKeysAsString()).on('change keydown keyup', function () {
            if (isTextSelected(this) === true) return null;

            var year = _this.getYearByProductKey(this.id);
            var product_type = _this.getProductTypeByProductKey(this.id);
            var product_type_max_value = _this.getProductTypeMax(product_type);

            var current_input_value = _this.extractIntegerValue(this.value);
            this.value = current_input_value;
            _this.updateProductQuantityObjectByProductKey(this.id, current_input_value);

            $(this).closest(".row").tooltip({'title': 'Maximum quantity exceeded: ' + product_type_max_value, 'trigger': 'manual'});
            var potential_product_type_in_year_total_quantity = _this.getProductTypeInYearTotalQuantity(product_type, year);
            if (potential_product_type_in_year_total_quantity == _this.validateValueWithinLimits(potential_product_type_in_year_total_quantity, 0, product_type_max_value)) {
                $(this).closest(".row").tooltip('hide');
                _this.validations_by_year_and_product_type[year][product_type] = true;
            } else {
                $(this).closest(".row").tooltip('show');
                _this.validations_by_year_and_product_type[year][product_type] = false;
            }

            _this.updateSubtotal(_this.ifFormValid());

            function isTextSelected(input){
                var startPos = input.selectionStart;
                var endPos = input.selectionEnd;
                var doc = document.selection;

                if(doc && doc.createRange().text.length != 0) return true;
                else if (!doc && input.value.substring(startPos,endPos).length != 0) return true;

                return false;
            }
        });

        $(_this.getProductKeysAsString()).on('focus', function () {
            if (this.value == 0) this.value = "";
        });

        $(_this.getProductKeysAsString()).on('blur', function () {
            if (this.value == "") this.value = 0;
        });

        /* Listen for form click on submit */
        $(_this.submit_button).click(function() {
            /*$(_this.order_form).submit();*/
            $.post( "includes/order_process.php", $(_this.order_form).serialize(), function(data) {
                console.log(data);
              
                if(data != "error") {
                    if ($("#checkout_temp").length == 0) $(".master_body").append('<span id="checkout_temp"></span>');
                    $("#checkout_temp").html(data);
                    $("#checkout_temp form").submit();
                    $("#checkout_temp").html("");
                } else alert("There has been a problem submitting your order. Please contact us directly.");
            }, "html").fail(function() {
                alert("There has been a problem submitting your order. Please contact us directly.");
            });
        });

        $(".clickable_preorder").click(function() {
            $(this).removeClass("clickable_preorder");
            $(".preorder_inactive").removeClass("preorder_inactive");
        });

        ///////////////////
        // TO BE REMOVED //
        ///////////////////

        this.tempFixForCanada = function() {
            $(".clickable_preorder").removeClass("clickable_preorder");
            $(".preorder_inactive").removeClass("preorder_inactive");
        }

        ///////////////////
        // TO BE REMOVED //
        ///////////////////
    }

    this.init();
}

$(function() {
    var ii_checkout = new BigPictureCheckout("order_form");
});
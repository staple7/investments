<?php
$page = "privacypolicy";

include("includes/start.php");
include("includes/head.php");
include("includes/master_header.php");
?>
        <div class="master_body">
            <div class="master_container">

                <div class="master_inner container">
                    <div class="row master_title">
                        <div>
                            <h1>Privacy Policy</h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <p><strong>What information do we collect?</strong></p>
                            <p>We collect information from you when you register on our site, place an order or fill out a form.</p>
                            <p>When ordering or registering on our site you may be asked to enter your: name, e-mail address, mailing address, phone number or credit card information. You may, however, visit our site anonymously.</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <p><strong>What do we use your information for?</strong></p>
                            <p>Any of the information we collect from you may be used in one of the following ways:</p>
                            <ul>
                                <li>To improve customer service &mdash; Your information helps us to more effectively respond to your customer service requests and support needs.</li>
                                <li>To process transactions &mdash; Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your consent, other than for the express purpose of delivering the purchased product or service requested.</li>
                                <li>To send periodic emails &mdash; The email address you provide for order processing, will only be used to send you information and updates pertaining to your order.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <p><strong>How do we protect your information?</strong></p>
                            <p>We implement a variety of security measures to maintain the safety of your personal information when you place an order.</p>
                            <p>We offer the use of a secure server. All supplied sensitive/credit information is transmitted via Secure Socket Layer (SSL) technology and then encrypted into our Payment gateway providers database only to be accessible by those authorized with special access rights to such systems, and are required to keep the information confidential.</p>
                            <p>After a transaction, your private information will not be stored on our servers.</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <p><strong>Do we use cookies?</strong></p>
                            <p>Yes, cookies are small files that a site or its service provider transfers to your computers hard drive through your Web browser (if you allow) that enables the sites or service providers systems to recognize your browser and capture and remember certain information.</p>
                            <p>We use cookies to help us remember and process the items in your shopping cart, understand and save your preferences for future visits and compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future. We may contract with third-party service providers to assist us in better understanding our site visitors. These service providers are not permitted to use the information collected on our behalf except to help us conduct and improve our business.</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <p><strong>Do we disclose any information to outside parties?</strong></p>
                            <p>We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <p><strong>California Online Privacy Protection Act Compliance</strong></p>
                            <p>Because we value your privacy we have taken the necessary precautions to be in compliance with the California Online Privacy Protection Act. We therefore will not distribute your personal information to outside parties without your consent.</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <p><strong>Children’s Online Privacy Protection Act Compliance</strong></p>
                            <p>We are in compliance with the requirements of COPPA (Children’s Online Privacy Protection Act), we do not collect any information from anyone under 13 years of age. Our website, products and services are all directed to people who are at least 13 years old or older.</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <p><strong>Online Privacy Policy Only</strong></p>
                            <p>This online privacy policy applies only to information collected through our website and not to information collected offline.</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <p><strong>Terms and Conditions</strong></p>
                            <p>Please also visit our Terms and Conditions section establishing the use, disclaimers, and limitations of liability governing the use of our website at <a href="http://investmentsillustrated.com">http://investmentsillustrated.com.</a></p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <p><strong>Your Consent</strong></p>
                            <p>By using our site, you consent to our web site privacy policy.</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <p><strong>Changes to our Privacy Policy</strong></p>
                            <p>If we decide to change our privacy policy, we will post those changes on this page.</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <p><strong>Contacting Us</strong></p>
                            <p>If there are any questions regarding this privacy policy you may contact us using the information below.</p>
                            <p><a href="mailto:info@investmentsillustrated.com">info@investmentsillustrated.com</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include("includes/master_footer.php"); ?>
    </body>
</html>

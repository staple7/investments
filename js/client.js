
define(['cookie', 'jqueryUI'], function(){


// without scrol when get focus for input;
 $.fn.focusWithoutScrolling = function(){
      var   x = window.scrollX,
            y = window.scrollY;
      this.focus();
      window.scrollTo(x, y);
  };
//$(document).load().scrollTop(0);
//window.onunload = function(){
//	window.scrollTo(0,0);
//};
//$("html, body").animate({ scrollTop: 0}, 100);


//**************hide option element:**********************//

jQuery.fn.toggleOption = function( show ) {
    jQuery( this ).toggle( show );
    if( show ) {
        if( jQuery( this ).parent( 'span.toggleOption' ).length )
            jQuery( this ).unwrap( );
    } else {
        if( jQuery( this ).parent( 'span.toggleOption' ).length == 0 )
            jQuery( this ).wrap( '<span class="toggleOption" style="display: none;" />' );
    }
};
//**************End hide option element:**********************//

var country  =  $.cookie('country');

//pro.html get select country by ip
function selectCountry(obj){
         var $pro_ip =   $('.version_country');
         var zipCode =	'Zip code';
                if(obj !==  'CA'){
                //this 'country' is global variable,its val get from cookie first,
                //if it is null or undefined, get val from getCountry() function and delivery
                //by 'cuntry' parameter;
                    $('body').removeClass('CA').addClass('US');
                    $('.currentCountry').val('US');
                    $pro_ip.find('#US').prop('checked',true).parent().addClass('semibold');
                    $pro_ip.find('#CA').parent().removeClass('semibold');
                    //switch pricing info
                    $('.USAinfo').show();
                    $('.CAinfo').hide();
                    $('optgroup.USAinfo').toggleOption(true);
                    $('optgroup.CAinfo').toggleOption(false);
                    $('optgroup.CAinfo').find('option').prop('selected', false);
                    zipCode = 'Zip code';


                 }
                 else{
                     $('body').removeClass('US').addClass('CA');
                     $('.currentCountry').val('CA');
                     $pro_ip.find('#CA').prop('checked',true).parent().addClass('semibold');
                     $pro_ip.find('#US').parent().removeClass('semibold');
                     //switch pricing info
                     $('.USAinfo').hide();
                     $('.CAinfo').show();
                     $('optgroup.CAinfo').toggleOption(true);
                     $('optgroup.USAinfo').toggleOption(false);
                     $('optgroup.USAinfo').find('option').prop('selected', false);
                     zipCode = 'Postal code';


                }
                //this for free login:
                //when geting expirated veirsion user checked 'remember me' from expiration page,
                //next time we will send this vale from free login from:
                $('input[name="openFree"]').each(function() {
                    $(this).val($.cookie('openFree'));
                });
                $('.shipping_zipCode').attr('placeholder',zipCode);
}

function getCountry(){
    if(country === null ){
        $.getJSON("https://geoip.nekudo.com/api/", function (data) {
            var country  =   data.country.code;
            $.cookie('country',country, {path: '/'});
            selectCountry(country);
            if(country == 'CA'){
                $('body').removeClass('US').addClass('CA');
            }
            else{
                $('body').removeClass('CA').addClass('US');
            }
        })
        .done(function() {
            console.log( "get ip Fn:success" );
        })
        .fail(function() {
            console.log( "get ip Fn:error" );
        })
        .always(function() {
            console.log( "get ip Fn:complete" );
        });
    }
    else{
        selectCountry(country);
    }

}
getCountry(country);


//version_country on pro page

$('.version_country label').click(function(){
    var selectCountryVal = $(this).attr('for');
    $(this).addClass('semibold').siblings('label').removeClass('semibold');
    $('input', this).val(selectCountry).attr('checked', true).parent().siblings('label').removeAttr('value').find('input').attr('checked', false);
    $.cookie('country', selectCountryVal);
    switchStatus(selectCountryVal);
    selectCountry(selectCountryVal);
});


 function switchStatus(obj){
        if(obj !==  'CA'){
            $('.availableCountries .US_ic').attr('data-status','selected').hide();
            $('.availableCountries .CA_ic').attr('data-status','').show();
            $('li[data-state="selectedLocation"]')
                .text('United States')
                .addClass('US_ic')
                .removeClass('CA_ic');


       }
       else{
            $('.availableCountries .CA_ic').attr('data-status','selected').hide();
            $('.availableCountries .US_ic').attr('data-status','').show();
            $('li[data-state="selectedLocation"]')
                .text('Canada')
                .addClass('CA_ic')
                .removeClass('US_ic');

       }
    }
	
$('footer').load('/template/footer.html .container', function(){


     //if $.cookie.country's val is empty:
    if(country === null || country === undefined){
        $.getJSON("https://geoip.nekudo.com/api/", function (data) {
        var country =   data.country.code;
            switchStatus(country);
            $.cookie('country', country, {path: '/'});
       });
    }
   //if $.cookie.country has val:
   else{
        switchStatus(country);
     }


    $('.selectCountry ul')
        .on('click','li[data-state="selectedLocation"]',function(){
            $(this).siblings('li').find('ul').slideToggle(100);

        })
        .on({
            click:function(event){
                event.stopPropagation();
                $(this).addClass('selectColor').siblings('li').removeClass('selectColor');
                $(this).parent().slideUp(100);
                $(this).attr('data-status','selected').hide();
                $(this).siblings('li').attr('data-status','').show();
                var changeCountry   =   $(this).attr('data-country');


                    if(changeCountry === 'US'){
                        $('li[data-state="selectedLocation"]').toggleClass('CA_ic US_ic').text('United States');

                     }
                     else{

                        $('li[data-state="selectedLocation"]').toggleClass('US_ic CA_ic').text('Canada');

                     }
                $.cookie('country', changeCountry, {path: '/'});
                selectCountry(changeCountry);
            }

        },'ul li');

        $('body').on({
            click:function(event){
                if($(event.target).closest('.selectCountry').length === 0){
                    $('.availableCountries').hide();
                }
            }
         });

        function cantact_bar_show(){
            var visitOnce	= $('.contact-sales').attr('data-once'),
                v;
            if(visitOnce === 'true'){
                v = 120000;
            }
            else{
                v = 120000;
            }
             $('.contact-sales').clearQueue().stop().delay(v).animate({
                'bottom':'-450px',
                'opacity':'1'
             }).attr('data-status','true');
        }

        function cantact_bar_hide(){
             $('.contact-sales').clearQueue().stop().animate({
                'bottom':'-520px',
                'opacity':'0'
            }).attr('data-status','false')
             .attr('data-content', 'hide');
        }

        function $switch_navbar(){
           var  $current_page   =   $('body').attr('data-status');
                  //console.log($current_page);
                  //for contact form

                  if($current_page === 'ContactForm'){
                     cantact_bar_show();
                  }
                  else{
                      cantact_bar_hide();
                  }

        }
        if($('body').is('.home')){
            var status = true;
            $(document).scroll(function() {
                var v = $(document).scrollTop();
                var contentStatus = $('.contact-sales').attr('data-content');
                if( (contentStatus === 'hide' && v > 70) || (contentStatus === 'show' && v > 500))  {
                    if(status){
                        cantact_bar_show();
                        status = false;
                        var visitOnce = $('.contact-sales').attr('data-once');
                        if(visitOnce !== 'true'){
                            $('.contact-sales').attr('data-once', 'true');
                        }
                    }

                }

                else{
                    if(!status){
                        cantact_bar_hide();
                        status = true;
                    }
                }
            });
        }
        else{
            $switch_navbar();
        }

});

function switchZipText(){
    //validate selection code:


    //if($.cookie('country') !== 'CA'){
//        $('.select_arrow option[val="CA"]').attr('selected', 'selected');
//        if($.cookie('country') === 'US'){
//            $('#zip').attr('placeholder','Billing ZIP');
//        }
//        else{
//            $('#zip').attr('placeholder','Postal Code');
//        }
//        $('#zip').attr('placeholder','Postal Code');
//    }
//
//    else{
//            $('.select_arrow option[val="CA"]').attr('selected', 'selected');
//            $('#zip').attr('placeholder','Postal Code');
//     }

    if($.cookie('country') === 'US'){
        $('#zip').attr('maxlength', 5);
    }
    else{
        $('#zip').attr('maxlength', 25);
    }
    $('.select_arrow').on('change',function(){
        if($(this).val() === 'US'){
            $('#zip').attr({'placeholder':'Billing ZIP',
                            'maxlength' : 5
                            });
        }
        else{
            $('#zip').attr({'placeholder':'Postal Code',
                            'maxlength' :   25
                      });
        }
    });
}

switchZipText();


function aboutus(){

    $('.business_card_header').on({
        'mouseenter mouseleave' : function(e){
            e.stopPropagation();
            var $this = $(this),
                currentStatus 	= $(this).attr('data-status'),
                quoteBody     	= $(".business_card_wrap[data-status='" + currentStatus + "']");
            if(e.type === 'mouseenter'){
                if(quoteBody.css('display') === 'none'){
                    $this.stop().animate({'backgroundColor':'#e0f1f8'},300)
                                .attr('data-display','block');
                    $this.parent().siblings()
                                  .find('.business_card_header')
                                  .stop().animate({'backgroundColor':'#F4F4F4'},300)
                                  .attr('data-display','none');
                    quoteBody.siblings('.business_card_wrap')
                             .stop().slideUp(300);
                    quoteBody.stop().slideDown(300);
                }
            }
            else{
                var w	 = $(document).width();
                if(w<750){
                    quoteBody.stop().slideUp(300);
                }

            }
        }
    })
}




function switchPaymentMethod(){
    $('.credit_cards, .paypal').on({
        click : function(){
            $(this).children('input').prop('checked', true);
            $(this).siblings().children('input').prop('checked', false);
        }
    })
}



    aboutus();
    switchPaymentMethod();


function detectScrollDistance(){
    if($('body').is('.home')){
        var ele	 	= $('.fullwidth'),
        eleH 	= ele.height(),
        navbar	= $('.navbar-fixed-top'),
        elementOffset = ele.offset().top,
        navbarH = navbar.height();
//		$('.scrollDown').animate({'bottom':'16px'}, 600);
        $(document).scroll(function() {
            var scrollTop	= $(window).scrollTop(),
                elementOffset = ele.offset().top,
                status		= navbar.attr('data-inFullScreen'),
                distance      = (elementOffset - scrollTop);
//				console.log(elementOffset);
                if(eleH + distance < navbarH){
                    if(status === 'true'){
                        navbar.delay().stop().animate({
                            backgroundColor: '#fff'
                        }, 500);

                        $('.navbar-brand').stop().animate({
                            color: '#58595B'
                        }, 500);

                        $('.signUpForFree, .premium').removeClass('hidden');
                        $('.login').removeClass('fullWidth_nav');
                        navbar.addClass('borderBottom').attr('data-inFullScreen', 'false');
                        console.log('gray');
                    }
                }
                else{
                    if(status === 'false'){
                        navbar.stop().animate({
                            backgroundColor: 'rgba(64,64,64,0.59)'
                        }, 500);
                        $('.navbar-brand').stop().animate({
                            color: '#ffffff'
                        }, 500);

                        $('.signUpForFree, .premium').addClass('hidden');
                        $('.login').addClass('fullWidth_nav');
                        console.log('white');
                        navbar.removeClass('borderBottom').attr('data-inFullScreen', 'true');
                    }
                }

        });

        $('.scrollDownBtn').click(function(){
            var navbarH = navbar.height();
            var h1 = $('.fullwidth').height();
            $("html, body").animate({ scrollTop: h1 -navbarH + 1}, 800);

        });
    }


}
    detectScrollDistance();


$(document)


    //contact sales
.on('click','.panel-heading',function(event){
        var $contact_form = $('.contact-sales');
        event.stopPropagation();
        //$('input',$contact_form).eq(0).focus();

        if( $contact_form.css('bottom')==='-450px'){
            $('.contact-sales-table').find('input:not(.btn),textarea').val('').removeClass('errorBorder').end().show();
            $('.submit_contactForm_succes').hide();
            $contact_form.clearQueue().stop().delay(50).animate({
                'bottom':'-22px'
            },300).attr('data-content', 'show');
            $('.glyphicon-menu-down', $contact_form).removeClass('invisible');
         }

         else{
             if($contact_form.attr('data-status') === 'true'){
                $contact_form.clearQueue().stop().delay(50).animate({
                    'bottom':'-450px'
                },300).attr('data-content', 'hide');
                $('.glyphicon-menu-down', $contact_form).addClass('invisible');
             }
         }

})


.on({
        click:function(event){

            if(!$(event.target).closest('.contact-sales').length){
                var $contact_form = $('.contact-sales');
                if($contact_form.css('bottom') ==='-15px' && $contact_form.attr('data-status') === 'true'){
                    $contact_form.clearQueue().stop().delay(100).animate({
                        'bottom':'-450px'
                    }).attr('data-content', 'hide');
                    $('.glyphicon-menu-down', $contact_form).addClass('invisible');
                }

            }
        }



    })



.on({

        mouseover:function(event){
            event.stopPropagation();
            var w	  = $(document).width();
            if(w<750){

                if($(event.target).closest('.business_card_header').length === 0 &&
                   $(event.target).closest('.business_card_wrap').length === 0){
                        $('.business_card_wrap').stop().slideUp(300);
                        $('.business_card_header').stop().animate({'backgroundColor':'#F4F4F4'},300);
                }
            }

            // animation for btn-warning

            if($(event.target).closest('.btn-warning').length === 0){
//                animationColor('.btn-warning', '#2dc57c', '#2BB673', 300);

            }

        }


    });


    function scaleEle(){

        function callFn(){
            var w	 = $(document).width();
            if(w<768){
            $('.business_card_header[data-status]').each(function(){
                    var $this = $(this),
                    currentStatus = $this.attr('data-status'),
                    quoteBody     = $(".business_card_wrap[data-status='" + currentStatus + "']");
                    $this.css({'background':'#F4F4F4'});
                    quoteBody.hide().insertAfter($this);
                });

                $('.switchTitleWithScreen').text('Rewards');

            }
            else{
                $('.business_card_header[data-status]').each(function(){
                    var $this = $(this),

                    currentStatus = $this.attr('data-status'),
                    quoteBody     = $(".business_card_wrap[data-status='" + currentStatus + "']"),
                    statusDisplay = $('.business_card_wrap:visible');
                    quoteBody.appendTo('.quotesContain');
                    if(statusDisplay.length === 0){

                        $('.business_card_header[data-status="quote1"]').stop().css({'background':'#e0f1f8'});
                        $('.business_card_wrap[data-status="quote1"]').slideDown(300);
                    }

                });
                 $('.switchTitleWithScreen').text('Referral Rewards');
            }
        }
        callFn();
        $(window).resize(function(){
            callFn();
         });
    }
  scaleEle();



  //business card showing

  (function(){
    var quotes = $(".quoteContainer");
    var quoteIndex = -1;

    function showNextQuote() {
        ++quoteIndex;
        quotes.eq(quoteIndex % quotes.length)
            .fadeIn(1000)
            .delay(5000)
            .fadeOut(1000, showNextQuote);
    }

    showNextQuote();

})();




   //fade in from bottom to top///
  function $slideTop(obj,value){
        var $obj	=	$(obj);
        if($obj.length){
            var obj_offset	=	$(obj).offset().top,
                distance	=	obj_offset - $(window).scrollTop();
            if(distance < value){
                $(obj).each(function(index) {
                    $(this).delay(300*index).animate({
                        'opacity':'1',
                        'margin-top':'0'
                    },800);
                });
            }
        }

  }

  $(window).scroll(function(){
        $slideTop('.ipad',800);

  });
  $slideTop('.ipad',800);
  //////animation for mouseover


    //validate payment form: from daypass account upgrade to premium
       (function $cookie() {
            var $user_email     =   $.cookie('email'),
                $firstname      =   $.cookie('firstname'),
                $lastname       =   $.cookie('lastname'),
                $plancode       =   $.cookie('plancode');
        //disable copy cut past from email input;
        $('.payment input[data-recurly="email"]').on('cut copy paste', function (e) {
            e.preventDefault();
        });

        if($.cookie('email') !== null){
            $('.payment').attr('data-status','upgrade');
            $('.payment input[data-recurly="email"]').val($user_email);
            $('.pro-form h1').attr('data-status','upgrade').text('Upgrade to Premium');
            $('.pro-form input[data-recurly="transaction"]').val('premium_upgrade');
            //get user email from cookie if this user logged in
            $('.forgot-password input[type="email"]').val($user_email);
        }
        if($plancode === 'big_picture_basic'){
           // $('.payment .email,.payment .password').hide();
           //$('.payment .confirm_email,.payment .upgrade_password').show();
        }
        if($firstname!== null){
            $('.payment input[data-roles="first_name"]').val($firstname);
        }
        if($lastname!== null){
            $('.payment input[data-roles="last_name"]').val($lastname);
        }
        else{
            $('.payment').attr('data-status','new-user');
            $('.pro-form h1').attr('data-status','upgrade').text('Get Big Picture Premium');
            $('.pro-form input[data-recurly="transaction"]').val('premium_new');
         }


       })();





//help center
$('div[data-status="helpCenter_leftList"]').load('/template/helpCenterList.html .callList', function(){
    var listID = $(this).attr('data-currentList');
    $('a',this).each(function() {
        if($(this).attr('data-list') === listID){
            $(this).addClass('gray');
        }
    });
});

//terms of use

$('div[data-status="terms_leftList"]').load('/template/termsList.html .callList', function(){
    var listID = $(this).attr('data-currentList');
    $('a',this).each(function() {
        if($(this).attr('data-list') === listID){
            $(this).addClass('gray');
        }
    });
});

function animationColor(obj, HoverCor, LeaveColor, T){
    $('body').on({

        mouseenter:function(){
            $(this).stop().animate({
                'backgroundColor':HoverCor,
                'borderColor':HoverCor
            },T);

        },
        mouseleave:function(){
            $(this).stop().animate({
                'backgroundColor':LeaveColor,
                'borderColor':LeaveColor
            },T);
        },
        click:function(){
            $(this).removeAttr( 'style' );
        }

    },obj);
}

//color animation:
animationColor('.facebook', '#3b579d', '#ACACAC', 300);
animationColor('.twitter', '#37baee', '#ACACAC', 300);
animationColor('.btn-warning', '#ffae00', '#ff9100', 300);
animationColor('.facebook_login', '#4164ac', '#3b5998', 300);
animationColor('.help_block', '#f0fafe', '#FAFAFA', 300);
// function btn_warning_colorController(){
//     $('.btn-warning').on({
//         click : function(){
//
//             $(this).animate({
//                'backgroundColor':'#ff9100',
//                'borderColor':'#ff9100'
//            },10);
//         },
//         mouseenter : function(){
//
//         }
//     });
// }
//
//btn_warning_colorController();
$('body')

    .on({
        click:function(e){
            e.stopPropagation();
            var $this       = $(this),
                $siblings   = $this.parent().siblings().find('.answer_block:visible');

            if($siblings.length > 0){
                $siblings.stop().slideUp(300, function(){
                    $this.next('.answer_block').slideToggle(300);
                });

            }
            else{
                $this.next('.answer_block').slideToggle(300);
            }

        }
    },'.helpCenter_contain h3')
    //Terms of Use animation
    .on({
        click:function(){
           //var $this   = $(this);
            $(this).next('.answer_block').slideToggle(300);
            $(this).parent().siblings().find('.answer_block').slideUp(300);
        }
    },'div[data-status="terms"] h3')

    .on({
        click:function(){
            var $id = $(this).attr('id');
            if($id.length !== 0){
               $.cookie('list_id',$id);
            }
        }
    },'div[data-status="helpCenter_recommendList"] a:not(.video_list a),.rollingPeriods')

    .on({
        click:function(){
//            $.removeCookie('list_id');
			$.cookie('list_id', null);
//			$.cookie('test123', 'test');
//			$.cookie('test123', null);
        }
    },'.help_center_home_btn,div[data-status="helpCenter_leftList"] a');

    $('.answer_block').each(function() {
        var list_id = $.cookie('list_id'),
            $id		= $(this).attr('id');
        if(list_id !== undefined && list_id === $id){
            $('.answer_block').hide();
            $('#' + list_id).show();

        }
    });



      /*
      *change style for selecting plan
      */
      function savePlanCode(obj, val){

         $(obj).click(function(){
            var date = new Date();
             var minutes = 10;
             date.setTime(date.getTime() + (minutes * 60 * 1000));
            if($(obj).hasClass('selectDaypass')){
                $.cookie('reactivatePlan', val, {expires: date,path: '/'});
            }
            else{
                $.cookie('reactivatePlan', val, {path: '/'});
            }

        });

      }


      savePlanCode('.reactivatePremium', 'reactivatePremium');
      savePlanCode('.reactivateDaypass', 'reactivateDaypass');
      savePlanCode('.selectDaypass', 'reactivateDaypass');

      $('.yearPlan, .dayPlan').each(function() {
          var thisPrice      = $(this).parent().next(),
              siblings       = $(this).parent().parent().siblings('.selectPlan'),
              anotherPlan    = siblings.find('label'),
              anotherPrice   = siblings.find('span').eq(1),
              currentPrice   = thisPrice.find('.price').text(),
              reactivatePlan = $.cookie('reactivatePlan');
            function switchInfo(obj){
                $(obj)
                    .add(thisPrice)
                    .addClass('semibold gray')
                    .removeClass('semi_grey')
                    .end().find('input')
                    .prop('checked', true);
                anotherPlan
                    .add(anotherPrice)
                    .addClass('semi_grey')
                    .removeClass('semibold gray')
                    .end().find('input')
                    .prop('checked', false);
                $('.amount span, .planPrice').text(currentPrice);
                if($(obj).hasClass('dayPlan')){
                    $('.daypassInfo').show();
                    $('.yearlyInfo').hide();

                }
                else{
                    $('.daypassInfo').hide();
                    $('.yearlyInfo').show();

                }
            }
            if(
                (reactivatePlan === 'reactivatePremium' && $(this).hasClass('yearPlan'))||
                (reactivatePlan === 'reactivateDaypass' && $(this).hasClass('dayPlan'))
            ){
                $(this)
                    .add(thisPrice)
                    .addClass('semibold gray')
                    .removeClass('semi_grey')
                    .end().find('input')
                    .prop('checked', true);
                anotherPlan
                    .add(anotherPrice)
                    .addClass('semi_grey')
                    .removeClass('semibold gray')
                    .end().find('input')
                    .prop('checked', false);
                $('.amount span, .planPrice').text(currentPrice);
                switchInfo(this);
            }

            $(this).click(function(){
                switchInfo(this);
            });

      });
//expiration page:
(function(){
    if($.cookie('email') === undefined || $.cookie('email') === null){
      //  $('.openFree').hide();
    }
    else{
      //  $('.openFree').show();
    }
    $('label[for="rememberMe"]').each(function() {
    console.log($.cookie('openFree'));
    if($.cookie('openFree') === 'true'){
            $('input',this).prop('checked', true);
        }
    });
})();

//referrals

    $('.referralRewardsTextArea').on('click',function(){
        $('.token-input', this).focus();
    });

    //get referral email from cookie

    $('input[name="referrer_email_id"]').each(function() {
        $(this).val($.cookie('referrer_email_id'));
    });


    function learnMoreAboutPoster(){
        var h	= $(document).height(),
            ele	= $('.freePoster_grayCover');
        ele.height(h).load('web_site_poster.html .container');

        function open_learnMoreAboutPoster(){
            $('.freePoster').on('click', function(e){                
                $('.freePoster_grayCover').height($(document).height());                                
                e.stopPropagation();
                ele.fadeIn(300);
            });
        }

        function close_learnMoreAboutPoster(){
            ele.on({
                click: function(){
                    ele.fadeOut(300);
                }
            }, '.posterFree_close')
				.on('click', function(e){
                if(!$(e.target).closest('.poster_container').length){
                    $(this).fadeOut(300);
                }
            });
        }

        open_learnMoreAboutPoster();
        close_learnMoreAboutPoster();

    }
	
	function teamDiscountPopUp(){
		var h	= $(document).height(),
			ele	= $('.team_discount_grayCover');
		ele.height(h).find('.team_discount_table_container').load('team_discount_table.html .discount_container');
		$('.open_team_discounts').on('click', function(e){
			$('.team_discount_grayCover').height($(document).height()).find('.discount_container').fadeIn(300);
			e.stopPropagation();
			ele.fadeIn(300);
		});
		
		function close_teamDiscountPopUp(){
            ele.on({
                click: function(){
                    ele.fadeOut(300);
                }
            }, '.teamDiscountsPopUp_close')
				.on('click', function(e){
					if(!$(e.target).closest('.teamDiscountPopUp_container').length){
						$(this).fadeOut(300);
					}
            });
        }

		close_teamDiscountPopUp();
		
		
	}
	
	teamDiscountPopUp();

    function shipMePoster(){
        var shipStatus = $('.shipMePoster input').prop('checked');
        $('#shipMePoster').on('click', function(e){
            e.stopPropagation();
            $('.shipMePoster_container').stop().slideToggle(300);
            $('.shipping_address, .shipping_city, .shipping_states, .shipping_zipCode').prop('required', function(index, attr){
                return attr = true ? false : true;
            });
        });
    }

    function teamDiscounts(){
		$('.team_discount_table_container').load('team_discount_table.html .discount_container', function(){
			
		});
        $('.teamDiscounts').on('click', function(){
            $('body,html').focusWithoutScrolling();
            $('.discount_container').stop().slideToggle(300);
        })
    }
    teamDiscounts();

    shipMePoster();

    learnMoreAboutPoster();






});

<?php
$page = "termsofuse";

include("includes/start.php");
include("includes/head.php");
include("includes/master_header.php");
?>
        <div class="master_body">
            <div class="master_container">

                <div class="master_inner container">
                    <div class="row master_title">
                        <div>
                            <h1>Terms of use</h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <p><strong>PLEASE READ THE TERMS AND CONDITIONS OF USE CAREFULLY BEFORE USING THIS SITE.</strong> By using this site, you agree to comply with and be bound by the following terms of use. After reviewing the following terms and conditions thoroughly, if you do not agree to the terms and conditions, please do not use this site.</p>
                            <ol class="order-lists">
                                <li><strong>Acceptance of Agreement.</strong> You agree to the terms and conditions outlined in this Terms and Conditions of use Agreement (Agreement) with respect to our site (the Site).</li>
                                <li><strong>Copyright.</strong> The content, organization, graphics, design, and other matters related to the Site are protected under applicable copyrights and other proprietary laws. The copying, reproduction, use, modification or publication of any such matters or any part of the Site is strictly prohibited, without our express prior written permission.</li>
                                <li><strong>Deleting and Modification.</strong> We reserve the right in our sole discretion, without any obligation and without any notice requirement, to edit or delete any documents, information or other content appearing on the Site, including this Agreement.</li>
                                <li><strong>Indemnification.</strong> You agree to indemnify, defend and hold us, our officers, our share holders, our partners, attorneys and employees harmless from any and all liability, loss, damages, claim and expense, including reasonable attorney's fees, related to your violation of this Agreement or use of the Site.</li>
                                <li><strong>Disclaimer.</strong>THE CONTENT, SERVICES, AND PRODUCT SAMPLES PUBLISHED ON THE SITE ARE PROVIDED FOR ILLUSTRATIVE PURPOSES ONLY. WE HAVE NO LIABILITY WHATSOEVER FOR YOUR USE OF ANY INFORMATION. THE INFORMATION AND ALL OTHER MATERIALS ON THE SITE ARE PROVIDED FOR GENERAL INFORMATION PURPOSES ONLY AND DO NOT CONSTITUTE PROFESSIONAL ADVICE. IT IS YOUR RESPONSIBILITY TO EVALUATE THE ACCURACY AND COMPLETENESS OF ALL INFORMATION AVAILABLE ON THIS SITE. The reproduction of part or all of this website without prior written consent from Investments Illustrated, Inc. is prohibited. The Big Picture, and the Investments Illustrated name and logo, are registered trademarks. Past performance is not an indicator of future performance.</li>
                                <li><strong>Limits.</strong> All responsibility or liability for any damages caused by viruses contained within the electronic file containing the form or document is disclaimed. We will not be liable to you for any incidental, special or consequential damages of any kind that may result from use of or inability to use the site.</li>
                                
                                <li><strong>Refunds .</strong> Orders cannot be returned. Orders are non-refundable. Exceptions may apply. Contact us for details. </li>
                                
                                <li><strong>Third-Party Website.</strong> All rules, terms and conditions, other policies (including privacy policies) and operating procedures of third-party linked websites will apply to you while on such websites. We are not responsible for the content, accuracy or opinions express in such Websites, and such Websites are not investigated, monitored or checked for accuracy or completeness by us. Inclusion of any linked Website on our Site does not imply approval or endorsement of the linked Website by us. This Site and the third-party linked websites are independent entities and neither party has authority to make any representations or commitments on behalf of the other. If you decide to leave our Site and access these third-party linked sites, you do so at your own risk.</li>
                                
                                
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include("includes/master_footer.php"); ?>
    </body>
</html>

<?php
$page = "contact";

include("includes/start.php");
require 'includes/ResponsiveCaptcha.php';
$captcha = new ResponsiveCaptcha();
include("includes/head.php");
include("includes/master_header.php");
?>
        <div class="master_body">
            <div class="master_container">
                <div class="master_inner container">

                    <div class="row master_title">
                        <div>
                            <h1>Contact us</h1>
                            <span class="lead">Please fill in the form below or <a href="mailto:info@investmentsillustrated.com">email us</a>.</span>
                        </div>
                    </div>

                    <form id="contact_form" class="form-horizontal" role="form" method="POST" action="includes/contact_process.php">
                        <div class="row contact_row">

                            <div id="form-success" class="alert alert-success" style="display: none">Thank you for your message. We will contact you shortly.</div>
                            <div id="form-info" class="alert alert-warning" style="display: none">Processing... Please wait.</div>
                            <div id="form-warning" class="alert alert-warning" style="display: none">There was an error sending the message. Please contact us directly.</div>
                            <div id="form-danger" class="alert alert-danger" style="display: none">Please correct the marked errors.</div>

                            <div class="form-group">
                                <label for="contact_name" class="control-label">Name&nbsp;*</label>
                                <div class="">
                                    <input type="text" class="form-control" id="contact_name" name="contact_name" maxlength="50" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_company" class="control-label">Company</label>
                                <div>
                                    <input type="text" class="form-control" id="company" name="company" maxlength="100" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_email" class="control-label">Email&nbsp;*</label>
                                <div>
                                    <input type="email" class="form-control" id="email" name="email" maxlength="50" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_message" class="control-label">Message&nbsp;*</label>
                                <div>
                                    <textarea class="form-control resizable" id="message" name="message" rows="5"  maxlength="500" required /></textarea>
                                </div>
                            </div>
                            <!--<div class="form-group">
                                <label for="captcha-field" class="control-label">Captcha&nbsp;*</label>
                                <div>
                                    <span id="captcha_question" for="captcha-field" class="help-block"><?php// echo $captcha->getNewQuestion() ?></span>
                                    <input type="text" id="captcha" name="captcha" class="form-control" id="captcha-field" required />
                                </div>
                            </div>-->
                            <div class="form-group">
                                <div>
                                    <input type="button" value="Send" id="submit" name="submit" class="btn btn-primary" />
                                </div>
                            </div>

                        </div>
                    </form>

                </div>
            </div>

            <div class="row master_title pb-4">
               
                <small>Investments Illustrated, Inc.<br>2625 Middlefield Road, #580<br> Palo Alto, CA 94306</small>
               
            </div>

        </div>

        <?php include("includes/master_footer.php"); ?>

        <script type="text/javascript"><?php include("js/$page.js"); ?></script>
    </body>
</html>

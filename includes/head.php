<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 



<html class="no-js"> <!--<![endif]-->
    
    <script type="application/ld+json">{
    "@context":"https://schema.org","@type":"WebSite","@id":"#website","url":"http://www.investmentsillustrated.com/","name":"Stock market chart | With its detailed illustrations of stock market history, the Big Picture chart helps you spark client conversations, set expectations, and grow assets under management.","potentialAction":{"@type":"SearchAction","target":"http://www.investmentsillustrated.com/?s={search_term_string}","query-input":"required name=search_term_string"}}</script>

<script type="application/ld+json">{"@context":"https://schema.org","@type":"Organization","url":"http://www.investmentsillustrated.com/","sameAs":[],"@id":"#organization","name":"Investments Illustrated Inc.","logo":"http://www.investmentsillustrated.com/img/logo/logo-48@2x.png"}</script>
    
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>The Big Picture by Investments Illustrated</title>
        <meta name="description" content="With its detailed illustrations of stock market history, the Big Picture chart helps you spark client conversations, set expectations, and grow assets under management.">
        <meta name="keywords" content="stock market chart, stock market history, S&P 500 history, stock market poster, financial charts, historical stock chart, andex chart, big picture chart, market history chart, investment poster">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="shortcut icon" href="favicon.ico" />
        <link rel="icon" href="favicon.ico" />
        <link href="apple-touch-icon.png" rel="apple-touch-icon" />
        <link href="apple-touch-icon-76x76.png" rel="apple-touch-icon" sizes="76x76" />
        <link href="apple-touch-icon-120x120.png" rel="apple-touch-icon" sizes="120x120" />
        <link href="apple-touch-icon-152x152.png" rel="apple-touch-icon" sizes="152x152" />

        <link rel="stylesheet" href="css/styles.css?v1.0.0">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <script type="text/javascript" src="//use.typekit.net/kuk5yqb.js"></script>
        <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    </head>
    <body class="<?php echo $page; ?>">
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->

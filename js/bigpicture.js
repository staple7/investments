/*
 * bigpicture.js
 */

(function() {
    /*
    @screen-xs:                  480px;
    @screen-sm:                  768px;
    @screen-md:                  992px;
    */
    var current_window_breakpoint = 0;
    var image_set = false;
    manageResize();

    function manageResize() {
        if (!document.getElementById("bppreview").complete && document.getElementById("bppreview").height < 200) {
            console.log("img still not loaded");
            window.requestAnimationFrame(manageResize);
        } else image_set = true;

        if (image_set == true) {
            var last_window_breakpoint = current_window_breakpoint;

            if (getWindowWidth() >= 992) current_window_breakpoint = 3;
            else if (getWindowWidth() >= 768) current_window_breakpoint = 2;
            else current_window_breakpoint = 1;

            if (current_window_breakpoint != last_window_breakpoint) resizeAction();
        }
    }

    function resizeAction() {
        $(".call-to-action").height("auto");
        $(".call-to-action").height(function() {
            if (getWindowWidth() >= 768) {
                var preview_height = $(this).parent().find( "img" ).outerHeight();
                if (preview_height > 250) return preview_height;
                else window.setTimeout(manageResize, 250);
            }
        });
        console.log("took resize action " + Date.now());
    }

    function getWindowWidth() {
        var windowWidth = 0;
        if (typeof(window.innerWidth) == 'number') {
            windowWidth = window.innerWidth;
        } else if (document.documentElement && document.documentElement.clientWidth) {
            windowWidth = document.documentElement.clientWidth;
        } else if (document.body && document.body.clientWidth) {
            windowWidth = document.body.clientWidth;
        }
        return windowWidth;
    }

    /* Manage resize event at an appropriate framerate => https://developer.mozilla.org/en-US/docs/Web/Reference/Events/resize */
    var optimizedResize=function(){function n(){r||(o=!0,i())}function i(){o?(o=!1,r=!0,e.forEach(function(n){n()}),window.requestAnimationFrame(i)):r=!1}function t(n){n&&e.push(n)}var e=[],o=!1,r=!1;return{init:function(i){window.requestAnimationFrame&&(window.addEventListener("resize",n),t(i))},add:function(n){t(n)}}}();
    optimizedResize.init(function() { manageResize(); });

    // http://paulirish.com/2011/requestanimationframe-for-smart-animating/
    // http://my.opera.com/emoller/blog/2011/12/20/requestanimationframe-for-smart-er-animating
    // requestAnimationFrame polyfill by Erik Möller. fixes from Paul Irish and Tino Zijdel
    (function(){var e=0;var t=["ms","moz","webkit","o"];for(var n=0;n<t.length&&!window.requestAnimationFrame;++n){window.requestAnimationFrame=window[t[n]+"RequestAnimationFrame"];window.cancelAnimationFrame=window[t[n]+"CancelAnimationFrame"]||window[t[n]+"CancelRequestAnimationFrame"]}if(!window.requestAnimationFrame)window.requestAnimationFrame=function(t,n){var r=(new Date).getTime();var i=Math.max(0,16-(r-e));var s=window.setTimeout(function(){t(r+i)},i);e=r+i;return s};if(!window.cancelAnimationFrame)window.cancelAnimationFrame=function(e){clearTimeout(e)}})()
})();

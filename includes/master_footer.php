<!-- FOOTER & ANALYTICS -->
<div class="master_footer">
    <div class="master_container">
        <div class="master_inner container">
            <div class="row">
                <div class="col-xs-<?php layout(); ?>">
                    <div class="advisor_logo mb-block">
                        <h4>Brought to you in partnership with:</h4>
                        <img src="img/logo/advisor.png">
                    </div>
                    <p>The Big Picture, and the Investments Illustrated name and logo, are registered trademarks.</p>
                    <p>&copy; <?php echo $ii_website->getCurrentYear(); ?> Investments Illustrated,&nbsp;Inc.</p>
                    <?php if (isset($_COOKIE['ii_c']) || isset($_GET["c"])) { ?>
                        <ul class="list-inline">
                            <li><a href="about.php">About us</a></li>
                            <li><a href="privacy-policy.php">Privacy policy</a></li>
                            <li><a href="terms-of-use.php">Terms of use</a></li>
                            <li><a href="index.php?resetlang">Change country</a></li>
                        </ul>
                    <?php } ?>
                    <div class="advisor_logo mb-none">
                        <p>Brought to you in partnership with:</p>
                        <img src="img/logo/advisor.png">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
<script src="js/plugins.js"></script>

<?php include_once("analyticstracking.php") ?>

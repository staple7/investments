<?php


include("website.php");

    
$ii_website = new Website();

if (isset($_GET["resetlang"])) {
    unset($_COOKIE['ii_c']);
}
if ($page == "index") {
    if (isset($_COOKIE['ii_c']) && in_array($_COOKIE['ii_c'], $ii_website->getRegionList())) {
        header("Location: bigpicture.php");
        exit();
    }
} else {
    
    if (isset($_GET["source"])){
        setcookie("ii_source",$_GET["source"], time()+60*60*24*30, "/", $_SERVER['HTTP_HOST']);
        if (strtolower($_GET["source"]) == "ap"){
            header("Location: bigpicture.php?c=us");
            exit();
        }
    }
    
    if (isset($_GET["c"])) 
        $ii_website->setCurrentRegion($_GET["c"]);
    elseif (isset($_COOKIE["ii_c"])) 
        $ii_website->setCurrentRegion($_COOKIE["ii_c"]);
    else {
        header("Location: http://".$_SERVER['HTTP_HOST']);
        exit();
    }

    if($_SERVER['HTTP_HOST'] == "localhost") setcookie("ii_c", $ii_website->getCurrentRegion());
    else setcookie("ii_c", $ii_website->getCurrentRegion(), time()+60*60*24*30, "/", $_SERVER['HTTP_HOST']);
}

function ret_layout($fraction = 1, $layout_cols = 36) {
    return round($layout_cols * $fraction);
}
function layout($fraction = 1) {
    echo ret_layout($fraction);
}
?>
<?php

#header("Location: " . "http://www.bigpicapp.co");
#die;

$page = "index";

include("includes/start.php");
include("includes/head.php");
?>
        <div class="master_header">
            <div class="master_container">
                <div class="master_inner container">
                    <div class="row">
                        <div class="col-xs-<?php layout(); ?> text-center">
                            <img class="masthead-brand" src="img/logo/logo-72@2x.png" width="255" height="72">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="master_body selectcountry_page">
            <div class="master_container">
                <div class="master_inner container text-center">
                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <h2><small>Please select a country</small></h2>
                            <ul class="list-inline">
                                <?php
                                    foreach ($ii_website->getRegionList() as $region) {
                                        echo '<li><a href="bigpicture.php?c=' . $region . '"><img src="img/flags/' . $region . 'flag-60@2x.png" width="60" height="60"></a></li>';
                                    }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php include("includes/master_footer.php"); ?>

        <script src="js/main.js"></script>
    </body>
</html>



define(['jqueryUI'], function(){
    
    var numberOfUsersElement = $("#numberOfUsers");
    numberOfUsersElement.on({
        keyup:function(e){
           var numberOfUsers = numberOfUsersElement.val();
            if (numberOfUsers.length==0) return;
            if (numberOfUsers==0){
                numberOfUsers = 1;
                numberOfUsersElement.val(numberOfUsers);
            }
            getPrice(numberOfUsers);
        },
        blur:function(e){
            var numberOfUsers = numberOfUsersElement.val();
            if (numberOfUsers.length==0){
                numberOfUsers = 1;
                numberOfUsersElement.val(numberOfUsers);
                getPrice(numberOfUsers);
            }
            getPrice(numberOfUsers);
        }
    });

    
    function getPrice(numberOfUsers){
        if (numberOfUsers.length==0||numberOfUsers==0){
            numberOfUsers = 1;
        }
        //console.log(numberOfUsers);

        var data = {
            numberOfUsers: numberOfUsers,
			coupon: $('#coupon').text(),
            requestFromBrowser: "yes"
        };
		console.log(data);
        $.ajax({
            type:'POST',
            dataType:"json",
            data:data,
            url:"/bigpicture_integrations/teams/get_team_price.php",
            success: function(data){
                console.log(data);
                showPrice(data.teamPrice);
                
            },
            error: function(errorcode){
                console.log(errorcode);
            }
        });
    }
    
    function showPrice(price){
        $("#teamPrice").html(price);
        
    }
   
});

<?php
// HEADER MENU
?>

<div class="master_header">
    <div class="master_container">
        <div class="master_inner container">
            <div class="row">
                <div class="col-xs-<?php layout(); ?>">
                    <div class="logo"><a href=<?php echo "\"/\"" ?>><img src="img/logo/logo-48@2x.png" width="170" height="48"></a></div>
                    <ul class="nav nav-pills">
                        <?php
                            if ($page != "checkout")
                                foreach ($ii_website->getNav() as $navElement)
                                    echo "<li".(($navElement["link"] == $page) ? " class=\"active\"" : "")."><a href=\"".$navElement["link"].".php\">".$navElement["name"]."</a></li>";
                        ?>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
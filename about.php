<?php
$page = "about";

include("includes/start.php");
include("includes/head.php");
include("includes/master_header.php");
?>
        <div class="master_body">
            <div class="master_container">

                <div class="master_inner container">
                    <div class="row master_title">
                        <div>
                            <h1>About us</h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-<?php layout(); ?>">
                            <!--<p><strong>Overview</strong></p>
                            <p>At Investments Illustrated we design charts that help financial professionals communicate key investment concepts to their clients.</p>
                            <p>Our charts combine expert financial analysis with smart design. We value:</p>
                            <ul>
                                <li>Meaning</li>
                                <li>Accuracy</li>
                                <li>Creativity</li>
                                <li>Simplicity</li>
                            </ul>
                            <p>Investments Illustrated, Inc.<br> 2625 Middlefield Road, #580<br> Palo Alto, CA<br> 94306. <br><br>Please contact us <a href="<?php// echo $ii_website->getNavLink('contact'); ?>.php">here</a>.</p>-->
        
				            <div class="aboutUsContainer">
				                <h4 class="semibold">Investments Illustrated, Inc.</h5>
				                <hr>
				                <br>
				                <p>
				                Based at our offices in Palo Alto, California, and B.C., Canada, we design charts that help financial professionals communicate key investment concepts to their clients.
				                </p>
				                <p class=" marginTop22">In addition to the Big Picture chart, we are the creators of the <a href="https://www.bigpicapp.co/" target="_blank">Big Picture application</a>, a next generation retirement planning application.</p>

				                <p>Please contact us <a href="<?php echo $ii_website->getNavLink('contact'); ?>.php">here</a>.</p>
				            </div>
        
                        </div>
                    </div>

					<div class="row thin text-center">
				        <div class="col-xs-<?php layout(); ?>">
				        	<h4 class="font_16 gray-light">OUR OFFICES</h4>
				            
				            <div class="outer rxGooglemap marginTop16">
					            <a title="Investments Illustrated, Inc." target="_blank" href="https://www.google.com/maps/place/2625+Middlefield+Rd+%23580,+Palo+Alto,+CA+94306,+USA/@37.434107,-122.1318249,17z/data=!3m1!4b1!4m5!3m4!1s0x808fbaf8bb5d2949:0x96f68c4809c754c2!8m2!3d37.4341028!4d-122.1296362">
					            <div class="inner googleMap">
					            	<img src="img/bp_previews/map-investment.png" style="width: 100%;">
					            </div>
					            </a>
				            </div>				            
				            <div>
				            <br>
				            <p class="h5" style="line-height:160%">
				            	Investments Illustrated, Inc.<br/>
								2625 Middlefield Road, #580<br/>
								Palo Alto, CA 94306
				            </p>				           
				            </div>				            
				        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include("includes/master_footer.php"); ?>
    </body>
</html>

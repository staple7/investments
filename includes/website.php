<?php

/*
 * Website Class
 * Loads website config and logic
 */

class Website
{
    /* PROPERTIES */
    private $configPath;
    private $config;
    private static $defaultConfigPath = "config.json";
    private static $regionsKeyword = "region";
    private static $productsKeyword = "productType";
    private static $languagesKeyword = "language";
    private static $pricingKeyword = "pricing";
    private static $productDisplayKeyword = "displayKey";
    private static $yearKeyword = "year";
    private static $navKeyword = "nav";
    private static $mapKeyword = "Map";
    
    /* CONSTRUCTORS */

    function __construct() {
        // if an argument was passed, it's a string, and it's not empty.
        // else initialize with $defaultConfigPath
        if (func_num_args() > 0 && is_string(func_get_arg(0)) && trim(func_get_arg(0)) !== '') $this->newConfig( func_get_arg(0) );
        else $this->newConfig( self::getDefaultConfigPath() );

        // sort the pricing property in ascending order
        $this->writeConfigProperty( self::getConfigPricingKeyword(), $this->sortPricing( $this->getPricing() ) );

        // make configuration maps
        $this->makeConfigArrayMaps();
    }


    /* CONFIG KEYS */
    private static function getDefaultConfigPath() {
        return self::$defaultConfigPath;
    }

    public static function getConfigRegionsKeyword() {
        return self::$regionsKeyword;
    }

    public static function getConfigProductsKeyword() {
        return self::$productsKeyword;
    }

    public static function getConfigLanguagesKeyword() {
        return self::$languagesKeyword;
    }

    public static function getConfigPricingKeyword() {
        return self::$pricingKeyword;
    }

    public static function getProductDisplayKeyword() {
        return self::$productDisplayKeyword;
    }

    public static function getYearKeyword() {
        return self::$yearKeyword;
    }

    public static function getNavKeyword() {
        return self::$navKeyword;
    }

    public static function getMapKeyword() {
        return self::$mapKeyword;
    }



    /* CONFIG-RELATED METHODS */
    
    /*
     * newConfig()
     * @desc establishes a new configuration. Should only be called by the constructor
     * @param string $path - path as string to be used for configuration
     * @return bool - success or failure
     */
    private function newConfig($path) {
        if ($this->setConfigPath($path))
            if ($this->setConfig( $this->getConfigDataFromConfigPath() ))
                return true;
        return false;
    }

    /*
     * setConfigPath()
     * @desc sets $configPath. Should only be called by newConfig()
     * @param string $path - string to be saved as $configPath
     * @return bool - success or failure
     */
    private function setConfigPath($path) {
        if (is_string($path) && $path != false) {
            $this->configPath = $path;
            return true;
        } else return false;
    }

    /*
     * setConfig()
     * @desc sets $config. Should only be called by newConfig()
     * @param array $configData - data to set
     * @return bool - success or failure
     */
    private function setConfig($configData) {
        if (is_array($configData) && !empty($configData)) {
            $this->config = $configData;
            return true;
        } else return false;
    }

    /*
     * getConfig()
     * @desc gets $config
     * @return associative array - $config
     */
    public function getConfig() {
        return $this->config;
    }

    /*
     * getConfigDataFromConfigPath()
     * @desc gets file contents using $configPath and parses to an associative array
     * @return array - empty on failure
     */
    private function getConfigDataFromConfigPath() {
        $file_contents = file_get_contents($this->configPath);
        if ($file_contents !== false) {
            $decodedData = json_decode($file_contents, TRUE);
            if (is_array($decodedData)) return $decodedData;
        }
        return [];
    }

    /*
     * makeConfigArrayMaps()
     * @desc makes maps from the configurations's array properties for easier access. Appends the "map" keyword to the property name
     * @return array - empty on failure
     */
    private function makeConfigArrayMaps() {
        foreach ($this->getConfig() as $configPropertyKey => $configProperty) {
            if (is_array($configProperty)) {
                $tempArray = [];
                foreach ($configProperty as $arrayIndex => $arrayObject) {
                    if (array_key_exists('code', $arrayObject))
                        $tempArray[$arrayObject['code']] = $arrayIndex;
                }
                $this->writeConfigProperty($configPropertyKey . self::getMapKeyword(), $tempArray);
            }
        }
    }

    /*
     * writeConfigProperty()
     * @desc makes a copy of $config, writes a property, and sets $config
     * @param string $key - property key to use
     * @param array $value - value to set for property
     * @return bool - success or failure
     */
    private function writeConfigProperty($key, $value) {
        $newConfig = $this->getConfig();
        $newConfig[$key] = $value;
        return $this->setConfig($newConfig);
    }

    /*
     * getConfigProperty()
     * @desc get a property from the $config object
     * @param string $key - key for property to retrieve
     * @return property value on success or undefined on failure
     */
    public function getConfigProperty($key) {
        $config = $this->getConfig();
        if (array_key_exists($key, $config)) return $config[$key];
        else return undefined;
    }
    


    /* GENERAL METHODS */
    public function getCartState() {
        return $this->getConfigProperty("cartState");
    }

    public function getCurrentYear() {
        return $this->getConfigProperty("currentYear");
    }
    
    public function getPreorderShift() {
        return $this->getConfigProperty("preorderShift");
    }

    public function getPreorderDiscount() {
        return $this->getConfigProperty("preorderDiscount");
    }

    public function getPreorderYear() {
        return $this->getCurrentYear() + $this->getPreorderShift();
    }

    public function setCurrentRegion($regionCode) {
        if (in_array($regionCode, $this->getRegionList()))
            $this->writeConfigProperty("currentRegion", $regionCode);
        return $this->getCurrentRegion();
    }

    public function getCurrentRegion() {
        return $this->getConfigProperty("currentRegion");
    }

    public function getYearDifference($regionCode="") {
        return $this->getCurrentYear() - $this->getRegionStartYear($regionCode);
    }

    /*
     * getDistinctPropertyValuesFromArray()
     * @desc get distinct values from a specific property in an array of associative arrays
     * @param array $array - array to iterate over. Should contain associative arrays
     * @param string $property - property to retrieve
     * @return an array with the distinct property values. Empty if none are found
     */
    private function getDistinctPropertyValuesFromArray($array, $property) {
        $distinctValues = [];
        foreach ($array as $value)
            if (!in_array($value[$property], $distinctValues))
                array_push($distinctValues, $value[$property]);
        return $distinctValues;
    }

    public function countLastBranch($array) {
        $count = 0;
        $this->countLastBranchHelper($array, $count);
        return $count;
    }

    private function countLastBranchHelper($array, &$count) {
        $alreadyCounted = false;
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $this->countLastBranchHelper($value, $count);
            } elseif ($alreadyCounted === false) {
                $count++;
                $alreadyCounted = true;
            }
        }
    }



    /* REGION-RELATED METHODS */
    public function getRegions() {
        return $this->getConfigProperty(self::getConfigRegionsKeyword());
    }

    public function getRegionMap() {
        return $this->getConfigProperty(self::getConfigRegionsKeyword() . self::getMapKeyword());
    }

    public function getRegionList() {
        return array_keys($this->getRegionMap());
    }

    public function getRegionByCode($regionCode="") {
        $regionCode = (empty($regionCode)) ? $this->getCurrentRegion() : $regionCode;
        return $this->getRegions()[$this->getRegionMap()[$regionCode]];
    }

    private function getRegionProperty($propertyKey,$regionCode="") {
        return $this->getRegionByCode($regionCode)[$propertyKey];
    }

    public function getRegionCode($regionCode="") {
        return $this->getRegionProperty("code",$regionCode);
    }

    public function getRegionCommonName($regionCode="") {
        return $this->getRegionProperty("commonName",$regionCode);
    }

    public function getRegionLongName($regionCode="") {
        return $this->getRegionProperty("longName",$regionCode);
    }

    public function getRegionDemonym($regionCode="") {
        return $this->getRegionProperty("demonym",$regionCode);
    }

    public function getRegionStartYear($regionCode="") {
        return $this->getRegionProperty("startYear",$regionCode);
    }

    public function getRegionProducts($regionCode="") {
        return $this->getRegionProperty("products",$regionCode);
    }

    public function getRegionProductTypes($regionCode="") {
        return $this->getDistinctPropertyValuesFromArray($this->getRegionProducts($regionCode), self::getConfigProductsKeyword());
    }

    public function getRegionLanguages($regionCode="") {
        return $this->getRegionProperty("languages",$regionCode);
    }

    public function getRegionCurrencyCode($regionCode="") {
        return $this->getRegionProperty("currencyCode",$regionCode);
    }

    public function getRegionProductsCombinationsAsKeys($regionCode="") {
        $combinations = [];
        $currentYear = $this->getCurrentYear();
        //for preorder $yearList = [$currentYear];
        $yearList = [$currentYear];
        foreach ($yearList as $year)
            foreach ($this->getRegionProducts($regionCode) as $regionProduct)
                foreach ($this->getRegionLanguages($regionProduct[self::getConfigRegionsKeyword()]) as $regionLanguage)
                    array_push($combinations, $this->getProductDisplayKey($year, $this->getRegionCode($regionProduct[self::getConfigRegionsKeyword()]), $regionProduct[self::getConfigProductsKeyword()], $regionLanguage));
                    
        return $combinations;
        
    }

    public function getRegionProductsCombinationsAsObjects($regionCode="") {
        $regionProductKeysList = $this->getRegionProductsCombinationsAsKeys($regionCode);
        foreach ($regionProductKeysList as &$regionProductKey)
            $regionProductKey = $this->getProductPropertiesfromDisplayKey($regionProductKey);   
        return $regionProductKeysList;
    }

    public function getRegionProductsCombinationsAsNestedObjects($rows=[], $columns=[], $regionCode="") {
        $combinationsMatrix = $this->getRegionProductsCombinationsAsObjects($regionCode);
        $distinctPropertyValuesList = [];

        foreach ($rows as $key => $value)
            $distinctPropertyValuesList[$key] = $this->getDistinctPropertyValuesFromArray($combinationsMatrix, $rows[$key]);
        $this->getRegionProductsCombinationsAsNestedObjectsHelper($combinationsMatrix, $distinctPropertyValuesList, $rows, $columns);
        return $combinationsMatrix;
    }

    private function getRegionProductsCombinationsAsNestedObjectsHelper(&$matrix, &$distinctPropertyValuesList, &$rows, &$columns, $i=0) {
        foreach ($matrix as $regionProductKey => $regionProduct) {
            foreach ($distinctPropertyValuesList[$i] as $distinctPropertyValue) {
                if (array_key_exists($rows[$i], $regionProduct) && $regionProduct[$rows[$i]] == $distinctPropertyValue) {
                    if (!array_key_exists($distinctPropertyValue, $matrix)) $matrix[$distinctPropertyValue] = [];
                    if ($i < count($rows)-1) array_push($matrix[$distinctPropertyValue], $regionProduct);
                    elseif (empty($columns))  array_push($matrix[$distinctPropertyValue], $regionProduct[self::getProductDisplayKeyword()]);
                    else
                        foreach ($columns as $column) {
                            if (in_array($column, $regionProduct)) {
                                $matrix[$distinctPropertyValue][$column] = $regionProduct[self::getProductDisplayKeyword()];
                            }
                        }
                    unset($matrix[$regionProductKey]);
                }
            }
        }

        if ($i < count($rows)-1)
            foreach ($matrix as $distinctPropertyKey => $distinctPropertyValue)
                $this->getRegionProductsCombinationsAsNestedObjectsHelper($matrix[$distinctPropertyKey], $distinctPropertyValuesList, $rows, $columns, $i+1);
    }



    /* PRODUCT-RELATED METHODS */
    public function getProductTypes() {
        return $this->getConfigProperty(self::getConfigProductsKeyword());
    }

    public function getProductTypesMap() {
        return $this->getConfigProperty(self::getConfigProductsKeyword() . self::getMapKeyword());
    }

    public function getProductTypesList() {
        return array_keys($this->getProductTypesMap());
    }

    public function getProductTypeByCode($productCode) {
        return $this->getProductTypes()[$this->getProductTypesMap()[$productCode]];
    }

    private function getProductTypeProperty($productCode,$propertyKey) {
        return $this->getProductTypeByCode($productCode)[$propertyKey];
    }

    public function getProductTypeName($productCode) {
        return $this->getProductTypeProperty($productCode, "name");
    }

    public function getProductTypeMax($productCode) {
        return $this->getProductTypeProperty($productCode, "max");
    }

    public function getProductTypeMaxByProductType() {
        $array = [];
        foreach ($this->getProductTypesList() as $productCode) {
            $array[$productCode] = $this->getProductTypeMax($productCode);
        }
        return $array;
    }

    public function getProductTypeHighestMax() {
        $highest = 0;
        foreach ($this->getProductTypesList() as $product)
            if ($this->getProductTypeMax($product) > $highest)
                $highest = $this->getProductTypeMax($product);
        return $highest;
    }

    public function getProductTypeHighestMaxByRegion($regionCode="") {
        $highest = 0;
        foreach ($this->getRegionProductTypes($regionCode) as $product) {
            if ($this->getProductTypeMax($product) > $highest)
                $highest = $this->getProductTypeMax($product);
        }
        return $highest;
    }

    public function getProductDisplayKey($year, $regionCode, $productCode, $languageCode) {
        return "bp_" . $productCode . "_" . $year . "_generic_" . $regionCode . "-" . $languageCode;
    }

    public function getDisplayKeyRegex() {
        return "/^bp_(" . implode("|", $this->getProductTypesList()) . ")_([\Amid\z-]?[\d]{4})_generic_(" . implode("|", $this->getRegionList()) . ")-(" . implode("|", $this->getLanguageList()) . ")$/i";
    }

    public function getProductPropertiesfromDisplayKey($displayKey) {
        preg_match($this->getDisplayKeyRegex(), $displayKey, $matches);
        return [ self::getProductDisplayKeyword() => $matches[0], self::getConfigProductsKeyword() => $matches[1], self::getYearKeyword() => $matches[2], self::getConfigRegionsKeyword() => $matches[3], self::getConfigLanguagesKeyword() => $matches[4] ];
    }

    public function isValidDisplayKey($displayKey) {
        return preg_match($this->getDisplayKeyRegex(), $displayKey);
    }



    /* LANGUAGE-RELATED METHODS */
    public function getLanguages() {
        return $this->getConfigProperty(self::getConfigLanguagesKeyword());
    }

    public function getLanguagesMap() {
        return $this->getConfigProperty(self::getConfigLanguagesKeyword() . self::getMapKeyword());
    }

    public function getLanguageList() {
        return array_keys($this->getLanguagesMap());
    }

    public function getLanguageByCode($languageCode) {
        return $this->getLanguages()[$this->getLanguagesMap()[$languageCode]];
    }

    private function getLanguageProperty($languageCode,$propertyKey) {
        return $this->getLanguageByCode($languageCode)[$propertyKey];
    }

    public function getLanguageCode($languageCode) {
        return $this->getLanguageProperty($languageCode, "code");
    }

    public function getLanguageName($languageCode) {
        return $this->getLanguageProperty($languageCode, "name")["en"];
    }



    /* PRICING-RELATED METHODS */
    private function sortPricing($pricing) {
        $sortedPricing = $pricing;
        usort($sortedPricing, function($a, $b) {
            return $a["code"] - $b["code"];
        });
        return $sortedPricing;
    }

    public function getPricing() {
        return $this->getConfigProperty(self::getConfigPricingKeyword());
    }

    public function getPricingBreakpoints() {
        return $this->getPricingBreakpointsListByRegion();
    }

    public function getPricingBreakpointsListByRegion($regionCode="") {
        $temp = [];
        foreach ($this->getPricing() as $index => $pricingNode)
            foreach ($this->getRegionProductTypes($regionCode) as $regionProduct)
                if (array_key_exists($regionProduct, $pricingNode))
                    $temp[$index] = $pricingNode["code"];
        return $temp;
    }

    public function getPricingBreakdown() {
        return $this->getPricingBreakdownByRegion();
    }

    public function getPricingBreakdownByRegion($regionCode="") {
        $breakdown = [];
        $breakpoints = $this->getPricingBreakpointsListByRegion($regionCode);
        $regionProductsList = $this->getRegionProductTypes($regionCode);

        for ($i = 0; $i < count($breakpoints); $i++) {
            $breakpointMax = (isset($breakpoints[$i+1])) ? $breakpoints[$i+1]-1 : $this->getProductTypeHighestMaxByRegion($regionCode);
            $temp = [ "min" => $breakpoints[$i], "max" => $breakpointMax ];
            foreach ($regionProductsList as $regionProduct)
                if($breakpointMax <= $this->getProductTypeMax($regionProduct))
                    $temp[$regionProduct] = $this->getPricing()[$i][$regionProduct];
            array_push($breakdown, $temp);
        }
        return $breakdown;
    }

    public function getProductTypeStartingPrice($productCode) {
        return $this->getPricing()[0][$productCode];
    }


    /* NAV-RELATED METHODS */
    public function getNav () {
        return $this->getConfigProperty(self::getNavKeyword());
    }

    public function getNavMap() {
        return $this->getConfigProperty(self::getNavKeyword() . self::getMapKeyword());
    }

    public function getNavList() {
        return array_keys($this->getNavMap());
    }

    public function getNavByCode($navCode) {
        return $this->getNav()[$this->getNavMap()[$navCode]];
    }

    private function getNavProperty($navCode,$navKey) {
        return $this->getNavByCode($navCode)[$navKey];
    }

    public function getNavLink($navCode) {
        return $this->getNavProperty($navCode, "link");
    }

    public function getNavName($navCode) {
        return $this->getNavProperty($navCode, "name");
    }
}

?>
<?php
include("website.php");
$ii_website = new Website("../config.json");
$ii_source = "ii";
if(isset($_COOKIE['ii_source'])){
   $ii_source =$_COOKIE['ii_source']; 
}
if (isset($_POST['c']) && in_array($_POST['c'], $ii_website->getRegionList())) {
    $regionCode = $_POST['c'];

    /* Extract valid products by key and then get their properties. */
    /* Also, count line items per product type and check there's no quantity mismatches */
    $receivedProducts = [];
    $productsByYear = [];
    $productsByProductType = [];
    $quantitiesByYearAndProductType = [];
    $lineItemsByYearAndProductType = [];

    $displayKeyKeyword = $ii_website->getProductDisplayKeyword();
    $productsKeyword = $ii_website->getConfigProductsKeyword();
    $yearKeyword = $ii_website->getYearKeyword();
    $preorderDiscount = $ii_website->getPreorderDiscount();
    

    foreach ($_POST as $fromPostKey => $fromPostValue) {
        if ($ii_website->isValidDisplayKey($fromPostKey) && $fromPostValue > 0) {
            $productProperties = $ii_website->getProductPropertiesfromDisplayKey($fromPostKey);
            $year = $productProperties[$yearKeyword];
            $productType = $productProperties[$productsKeyword];
            $displayKey = $productProperties[$displayKeyKeyword];

            if (!array_key_exists($year, $productsByYear)) $productsByYear[$year] = [];            
            if (!array_key_exists($productType, $productsByProductType)) $productsByProductType[$productType] = [];

            if (!array_key_exists($year, $quantitiesByYearAndProductType)) $quantitiesByYearAndProductType[$year] = [];
            if (!array_key_exists($productType, $quantitiesByYearAndProductType[$year]))
                $quantitiesByYearAndProductType[$year][$productType] = 0;

            if (!array_key_exists($year, $lineItemsByYearAndProductType)) $lineItemsByYearAndProductType[$year] = [];
            if (!array_key_exists($productType, $lineItemsByYearAndProductType[$year]))
                $lineItemsByYearAndProductType[$year][$productType] = 0;
            
            if ($quantitiesByYearAndProductType[$year][$productType] + $fromPostValue <= $ii_website->getProductTypeMax($productType)) {
                $productsByYear[$year][] = $displayKey;
                $productsByProductType[$productType][] = $displayKey;
                $quantitiesByYearAndProductType[$year][$productType] += $fromPostValue;
                $lineItemsByYearAndProductType[$year][$productType]++;

                $productProperties["quantity"] = $fromPostValue;
                $receivedProducts[] = $productProperties;
            } else {
                echo "error"; /* Quantities mismatch */
                exit();
            }
        }
    }
 
    if (is_array($receivedProducts) && count($receivedProducts) > 0) {
        $result = '<form id="process_form" method="POST" action="https://sites.fastspring.com/investmentsillustrated/api/order?language=en&country='.strtoupper($regionCode).'&referrer='.strtolower($ii_source).'&currency='.strtoupper($ii_website->getRegionCurrencyCode($regionCode)).'">';
        $result .= '<input type="hidden" name="operation" value="create" />';
        $result .= '<input type="hidden" name="destination" value="checkout" />';
        $result .= '<input type="hidden" name="source" value="'.strtoupper($ii_source).'" />';
        /*$result .= '<input type="hidden" name="mode" value="test" />';*/
        
        /* TAGS */
        $tags = "";
        /* Unit Price */
        foreach (array_keys($productsByYear) as $year) {
            $yearNickname = ($year == $ii_website->getCurrentYear()) ? "current" : "preorder";
            foreach (array_keys($productsByProductType) as $productType) {
                foreach ($ii_website->getPricingBreakdown() as $pricingBreakdownNode) {
                    if (array_key_exists($year, $quantitiesByYearAndProductType)
                        && array_key_exists($productType, $quantitiesByYearAndProductType[$year])
                        && array_key_exists($productType, $pricingBreakdownNode)
                        && $quantitiesByYearAndProductType[$year][$productType] >= $pricingBreakdownNode['min']
                        && $quantitiesByYearAndProductType[$year][$productType] <= $pricingBreakdownNode['max']) {
                        $tags .= ',' . $productType . '_' . $yearNickname . '_unit_price=';
                        $tags .= round(($year == $ii_website->getCurrentYear()) ? ($pricingBreakdownNode[$productType] * 100) : ($pricingBreakdownNode[$productType] * (1 - $preorderDiscount) * 100));
                        $tags .= ',' . $productType . '_' . $yearNickname . '_line_items=' . $lineItemsByYearAndProductType[$year][$productType];
                        $tags .= ',' . $productType . '_' . $yearNickname . '_quantity=' . $quantitiesByYearAndProductType[$year][$productType];
                    }
                }
            }
        }
        
         $tags .= ',source=' . strtolower($ii_source);

        /* Print tags */
        /* Tag "cs" loads custom style with "continue shopping" translations. See ticket #45548 in FastSpring. */
        $result .= '<input type="hidden" name="tags" value="cs' . $tags . '" />';

        foreach ($receivedProducts as $index => $receivedProduct) {
            $result .= '<input type="hidden" name="product_'. ($index+1) .'_path" value="/' . $receivedProduct[$ii_website->getProductDisplayKeyword()] . '" />';
            $result .= '<input type="hidden" name="product_'. ($index+1) .'_quantity" value="' . $receivedProduct['quantity'] . '" />';
        }

        echo $result . "</form>"; /* Success! Print the form! */

    } else echo "error"; /* No valid products received */
} else echo "error"; /* Hidden region atribute not set or doesn't match any of the configured regions */
?>